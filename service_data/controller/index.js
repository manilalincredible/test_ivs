var mongo = require('../datasource/mongodbs.js');
var ObjectID = require('mongodb').ObjectID;

var documentType = {
    Single: 0,
    Collection: 1
};

module.exports = {
    readData: function (query, callback) {
        if (query.options.docType === documentType.Collection) {
            if (query.query !== null && query.query !== undefined) {
                if (query.query._id !== undefined && query.query._id !== null && !Array.isArray(query.query._id)) {
                    query.query._id = ObjectID(query.query._id);
                }
            }
            if (Array.isArray(query.query._id)) {
                var arrayofObjectid = [];
                for (var m = 0; m < query.query._id.length; m++) {
                    arrayofObjectid.push(ObjectID(query.query._id[m]));
                }
                query.query = {
                    "_id": {
                        $in: arrayofObjectid
                    }
                };
            }
            mongo.findDocuments(query.url, query.db, query.table, query.query, function (err, data) {
                callback(err, data);
            }, query.selector, query.options.limit, query.options.order);
        } else if (query.options.docType === documentType.Single) {
            if (query.query !== null && query.query !== undefined) {
                if (query.query._id !== undefined && query.query._id !== null) {
                    query.query._id = ObjectID(query.query._id);
                }
            }
            mongo.findDocument(query.url, query.db, query.table, query.query, function (err, data) {
                callback(err, data);
            }, query.selector);
        } else {
            callback("Invalid Call", null);
        }
    },
    writeData: function (query, callback) {
        if (query.options.docType == documentType.Single) {
            if (typeof query.query._id === "undefined" && typeof query.keyIdentifier === "undefined") {
                mongo.insertDocument(query.url, query.db, query.table, query.query, function (err, data) {
                    callback(err, data);
                });
            } else {
                mongo.updateDocument(query.url, query.db, query.table, query.query, function (err, data) {
                    callback(err, data);
                }, query.keyIdentifier);
            }
        } else if (query.options.docType == documentType.Collection) {
            var data = query.query;
            this.writeColletionData(data, 0, query, function (err, result) {
                sendResponse(res, result, err);
            }, query.keyIdentifier);
        } else {
            callback("Invalid Call", null);
        }
    },
    writeColletionData: function (dataArray, counter, query, callback, keyIdentifier) {
        if (dataArray && counter < dataArray.length) {
            var data = dataArray[counter];
            if (data._id === "undefined" && typeof keyIdentifier === "undefined") {
                mongo.insertDocument(query.url, query.db, query.table, data, function (err, data) {
                    if (err !== null) {
                        callback(err, result);
                    } else {
                        counter++;
                        writeColletionData(dataArray, counter, query, callback, keyIdentifier);
                    }
                });
            } else {
                mongo.updateDocument(query.url, query.db, query.table, data, function (err, data) {
                    if (err !== null) {
                        callback(err, result);
                    } else {
                        counter++;
                        writeColletionData(dataArray, counter, query, callback, keyIdentifier);
                    }
                }, keyIdentifier);
            }
        } else {
            callback(null, { success: true });
        }
    },
    getCount: function (query, callback) {
        mongo.getCount(query.url, query.db, query.table, query.query, function (err, data) {
            callback(err, data);
        });
    },
    aggregate: function (query, callback) {
        mongo.aggregate(query.url, query.db, query.table, query.query, function (err, data) {
            data = (!err && query.options.docType == documentType.Single && data.length > 0) ? data[0] : data;
            callback(err, data);
        });
    },
}